import 'dart:html';
import 'package:polymer/polymer.dart';
import 'package:paper_elements/paper_button.dart';

@CustomTag('my-button')
class MyButton extends PolymerElement {
  @observable var title = null;
  PaperButton removeButton = addRemoveButton('Remove button', 'remove_button');

  // setting the polymer element's constructor
  MyButton.created() : super.created();

  // declaring the events required from the html - add_btn
  void clicked(Event e, var detail, Node node) {
    print('Paper button was clicked');

    //change the title text;
    node.text = 'This was clicked';

    // append it to the screen
    node.parentNode.append(removeButton);

    // set the listeners afterwards
    removeButton.onClick.listen((e) {
      removeButton.remove();
      node.text = 'Click here, again';
    });
  }

  // registering the typing method to record every keypressup stroke
  void typing(Event e, var detail, Node node) {
    // parse the input_1 as an InputElement instead of a Node
    var input_1 = (node as InputElement);

    // print out the messages
    print(input_1.value);

    // give that content to the title
    title = input_1.value;
  }
}

PaperButton addRemoveButton(String text, String id) {
  // creates the Button with text and id
  return new PaperButton()
    ..text = text
    ..id = id
    ..classes.add("remove-button");
}
